﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Excel = Microsoft.Office.Interop.Excel;
using System.Linq;

namespace ExcelSerializerTest
{
    [TestFixture]
    public class ExcelLearningTest
    {
        [Test]
        public void ExcelReadFileTest()
        {
            var exclApp = new Excel.Application();
            exclApp.Visible = true;
            string pathfile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\Book1.xlsx");
            var exclBook = exclApp.Workbooks.Open(pathfile);
            var exclSheet = exclBook.Sheets[1];
            var lastRow = exclSheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell).Row;
            Assert.AreEqual(3, lastRow);
            exclApp.Quit();
        }

        [Test]
        public void ExcelReadFileNameTest()
        {
            var exclApp = new Excel.Application();
            exclApp.Visible = true;
            string pathfile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\Book1.xlsx");
            var exclBook = exclApp.Workbooks.Open(pathfile);
            Assert.AreEqual("Book1.xlsx", exclBook.Name);
            exclApp.Quit();
        }

        [Test]
        public void ExcelReadFileSheetNameTest()
        {
            var exclApp = new Excel.Application();
            exclApp.Visible = true;
            string pathfile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\Book1.xlsx");
            var exclBook = exclApp.Workbooks.Open(pathfile);
            var exclSheet = (Excel.Worksheet)exclBook.Sheets[1];
            Assert.AreEqual("Sheet1", exclSheet.Name);
            exclApp.Quit();
        }

        [Test]
        public void ExcelReadSheet1FileTest()
        {
            var exclApp = new Excel.Application();
            exclApp.Visible = true;
            string pathfile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\Book1.xlsx");
            var exclBook = exclApp.Workbooks.Open(pathfile);
            int page = 1;
            for(int i=1; i <= exclBook.Sheets.Count; i++)
            {
              if(((Excel.Worksheet)exclBook.Sheets[i]).Name == "Sheet1")
                {
                    page = i;
                    break;
                }
            }
            var exclSheet = (Excel.Worksheet)exclBook.Sheets[page];
            var lastRow = exclSheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell).Row;
            IList <Sheet1> sheet1 = new List<Sheet1>();
            for (int i = 2; i <= lastRow; i++)
            {
                System.Array exclValues = (System.Array)exclSheet.get_Range("A" + i.ToString(), "C" + i.ToString()).Cells.Value;
                sheet1.Add(new Sheet1
                {
                    Field1 = int.Parse(exclValues.GetValue(1, 1).ToString()),
                    Field2 = exclValues.GetValue(1,2).ToString(),
                    Field3 = decimal.Parse(exclValues.GetValue(1, 3).ToString())
                });
            }
            Assert.AreEqual(2, sheet1.Count);
            Assert.AreEqual(1, sheet1[0].Field1);
            exclApp.Quit();
        }

        [Test]
        public void ExcelReadSheet1PropertyFileTest()
        {
            var exclApp = new Excel.Application();
            exclApp.Visible = true;
            string pathfile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\Book1.xlsx");
            var exclBook = exclApp.Workbooks.Open(pathfile);
            int page = 1;
            for (int i = 1; i <= exclBook.Sheets.Count; i++)
            {
                if (((Excel.Worksheet)exclBook.Sheets[i]).Name == "Sheet1")
                {
                    page = i;
                    break;
                }
            }
            var exclSheet = (Excel.Worksheet)exclBook.Sheets[page];
            var lastRow = exclSheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell).Row;
            var lastColumn = exclSheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell).Column;

            Type typeSheet1 = typeof(Sheet1);
            var props = typeSheet1.GetProperties();
            Dictionary<int, PropertyInfo> intProperty = new Dictionary<int, PropertyInfo>();
            for(int i = 1; i <= lastColumn; i++)
            {
                string headerName = exclSheet.Cells[1, i].Value.ToString();
                var prop = props.FirstOrDefault(p => p.Name == headerName);
                if (prop == null) continue;
                intProperty.Add(i, prop);
            }



            IList<Sheet1> sheet1 = new List<Sheet1>();
            for (int i = 2; i <= lastRow; i++)
            {
                for(int j = 1; j<= lastColumn; j++)
                {

                }
                System.Array exclValues = (System.Array)exclSheet.get_Range("A" + i.ToString(), "C" + i.ToString()).Cells.Value;
                sheet1.Add(new Sheet1
                {
                    Field1 = int.Parse(exclValues.GetValue(1, 1).ToString()),
                    Field2 = exclValues.GetValue(1, 2).ToString(),
                    Field3 = decimal.Parse(exclValues.GetValue(1, 3).ToString())
                });
            }
            Assert.AreEqual(2, sheet1.Count);
            Assert.AreEqual(1, sheet1[0].Field1);
            exclApp.Quit();
        }

        [Test]
        public void ExcelReadDateTest()
        {
            var exclApp = new Excel.Application();
            exclApp.Visible = true;
            string pathfile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\Book1.xlsx");
            var exclBook = exclApp.Workbooks.Open(pathfile);
            int page = 1;
            for (int i = 1; i <= exclBook.Sheets.Count; i++)
            {
                if (((Excel.Worksheet)exclBook.Sheets[i]).Name == "Sheet2")
                {
                    page = i;
                    break;
                }
            }
            var exclSheet = (Excel.Worksheet)exclBook.Sheets[page];

            string dateName = exclSheet.Cells[2, 3].Value.ToString();
            
            Assert.AreEqual("01.05.1997 0:00:00", dateName);
            exclApp.Quit();
        }

    }

}
