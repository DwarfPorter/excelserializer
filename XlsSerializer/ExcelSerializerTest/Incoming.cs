﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelSerializerTest
{
    public class Incoming
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public decimal Qnty { get; set; }
        public decimal Price { get; set; }
        public decimal Amount { get; set; }
    }
}
