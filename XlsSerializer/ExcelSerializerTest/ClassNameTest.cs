﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using XlsSerializer.Reflector;

namespace ExcelSerializerTest
{
    [TestFixture]
    class ClassNameTest
    {
        [Test]
        public void ClassName_Test()
        {
            var className = new ClassName<ForClassNameTest>();
            Assert.IsNotNull(className);
        }
        
        [Test]
        public void ClassName_Name_Test()
        {
            var className = new ClassName<ForClassNameTest>();
            Assert.AreEqual("ForClassNameTest", className.Name);
        }
    }


    class ForClassNameTest { }
}
