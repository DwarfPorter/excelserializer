﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Excel = Microsoft.Office.Interop.Excel;
using System.Linq;
using XlsSerializer;
using XlsSerializer.ExcelWrapper;

namespace ExcelSerializerTest
{
    [TestFixture]
    class ExcelWrpTest
    {

        [Test]
        public void ExcelWrp_Test()
        {
            string pathfile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\Book1.xlsx");
            using (IExcelWrp excel = new ExcelWrp(pathfile))
            {
                Assert.IsNotNull(excel);
            }
        }

        [Test]
        public void ExcelWrp_GetSheets_Test()
        {
            string pathfile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\Book1.xlsx");
            using (IExcelWrp excel = new ExcelWrp(pathfile))
            {
                IEnumerable<string> sheets = excel.GetSheets();
                Assert.AreEqual(2, sheets.Count());
                Assert.AreEqual("Sheet1", sheets.First());
            }
        }

        [Test]
        public void ExcelWrp_GetSheets_SecondInvoke_Test()
        {
            string pathfile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\Book1.xlsx");
            using (IExcelWrp excel = new ExcelWrp(pathfile))
            {
                IEnumerable<string> sheets = excel.GetSheets();
                sheets = excel.GetSheets();
                Assert.AreEqual(2, sheets.Count());
                Assert.AreEqual("Sheet1", sheets.First());
            }
        }


        [Test]
        public void ExcelWrp_GetFileName_Test()
        {
            string pathfile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\Book1.xlsx");
            using (IExcelWrp excel = new ExcelWrp(pathfile))
            {
                string actual = excel.GetFileName();
                Assert.AreEqual("Book1.xlsx", actual);
            }
        }

        [Test]
        public void ExcelWrp_GetSheet_Test()
        {
            string pathfile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\Book1.xlsx");
            using (IExcelWrp excel = new ExcelWrp(pathfile))
            {
                int actual = excel.GetSheet("Sheet2");
                Assert.AreEqual(1, actual);
            }
        }

        [Test]
        public void ExcelWrp_GetSheet_Exception_Test()
        {
            string pathfile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\Book1.xlsx");
            using (IExcelWrp excel = new ExcelWrp(pathfile))
            {
                Assert.Throws<ArgumentOutOfRangeException>(delegate { excel.GetSheet("MySheet"); });
            }
        }

        [Test]
        public void ExcelWrp_GetCell_Test()
        {
            string pathfile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\Book1.xlsx");
            using (IExcelWrp excel = new ExcelWrp(pathfile))
            {
                string actual = excel.GetCell("Sheet1", 1, 0);
                Assert.AreEqual("1", actual);
                
            }
        }

        [Test]
        public void ExcelWrp_GetCell_Exception_Test()
        {
            string pathfile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\Book1.xlsx");
            using (IExcelWrp excel = new ExcelWrp(pathfile))
            {
                Assert.Throws<System.InvalidOperationException>(delegate { excel.GetCell("Sh2eet1", 1, 0); });
            }
        }

        [Test]
        public void ExcelWrp_GetCell1fieldName_Test()
        {
            string pathfile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\Book1.xlsx");
            using (IExcelWrp excel = new ExcelWrp(pathfile))
            {
                string actual = excel.GetCell("Sheet1", "Field2", 1);
                Assert.AreEqual("String1", actual);
            }
        }

        [Test]
        public void ExcelWrp_GetCell1fieldName_Exception_Test()
        {
            string pathfile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\Book1.xlsx");
            using (IExcelWrp excel = new ExcelWrp(pathfile))
            {
                Assert.Throws<System.ArgumentOutOfRangeException>(delegate { excel.GetCell("Sheet1", "1", 0); });
            }
        }

        [Test]
        public void ExcelWrp_CountRow_Test()
        {
            string pathfile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\Book1.xlsx");
            using (IExcelWrp excel = new ExcelWrp(pathfile))
            {
                Assert.AreEqual(2, excel.CountRow("Sheet1"));
            }
        }

    }
}
