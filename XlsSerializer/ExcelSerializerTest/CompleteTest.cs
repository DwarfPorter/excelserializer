﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XlsSerializer;

namespace ExcelSerializerTest
{
    [TestFixture]
    class CompleteTest
    {
        [Test]
        [TestCaseSource(typeof(ProviderData), nameof(ProviderData.TestCases), new object[] { @"..\..\Book1.xlsx" })]
        public void Complete_Test(Sheet1 sheet1)
        {
            Assert.IsNotNull(sheet1);
            Assert.IsNotEmpty(sheet1.Field2);
            Assert.Greater(sheet1.Field1, 0);
        }
    }

    class ProviderData
    {
        public static IEnumerable<Sheet1> TestCases(string excelName)
        {
            var answer = new List<Sheet1>();
            string pathfile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, excelName);

            using (var xlsxSerializer = new XlsxSerializer(pathfile))
            {
                return xlsxSerializer.DeserializeList<Sheet1>();
            }
        }
    }
}
