﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelSerializerTest
{
    public class Sheet2
    {
        public int F1 { get; set; }
        public string F2 { get; set; }
        public DateTime F3 { get; set; }
    }
}
