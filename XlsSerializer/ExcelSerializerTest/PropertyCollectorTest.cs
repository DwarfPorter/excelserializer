﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using XlsSerializer.Reflector;
using System.Reflection;

namespace ExcelSerializerTest
{
    [TestFixture]
    class PropertyCollectorTest
    {
        [Test]
        public void PropertyCollector_Test()
        {
            var propertyCollector = new PropertyCollector<ForPropertyCollectionTest>();
            Assert.IsNotNull(propertyCollector);
        }

        [Test]
        public void PropertyCollector_Properties_Test()
        {
            var propertyCollector = new PropertyCollector<ForPropertyCollectionTest>();
            IEnumerable<PropertyInfo> properties = propertyCollector.Properties;
            Assert.AreEqual(2, properties.Count());
            Assert.AreEqual("Field1", properties.First().Name);
        }
    }

    class ForPropertyCollectionTest
    {
        public int Field1 { get; set; }
        public string Field2 { get; set; }
        public decimal Field3 { get; }
        private int Field4 { get; set; }
        public string Field5 { get; private set; }
    }
}
