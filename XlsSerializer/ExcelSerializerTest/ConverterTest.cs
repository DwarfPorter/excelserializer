﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using XlsSerializer.Reflector;
using XlsSerializer.Converter;

namespace ExcelSerializerTest
{
    [TestFixture]
    class ConverterTest
    {
        [Test]
        public void ConverterInt_Test()
        {
            IConverter converterInt = new ConverterInt();
            Assert.AreEqual(42, converterInt.Convert("42"));

        }

        [Test]
        public void ConverterString_Test()
        {
            IConverter converter = new ConverterString();
            Assert.AreEqual("SuperNumber", converter.Convert("SuperNumber"));
        }

        [Test]
        public void ConverterDecimal_Test()
        {
            IConverter converter = new ConverterDecimal();
            Assert.AreEqual((decimal)42.42, converter.Convert((42.42).ToString()));
        }

        [Test]
        public void ConverterBoolean_Test()
        {
            IConverter converter = new ConverterBoolean();
            Assert.AreEqual(true, converter.Convert("true"));
            Assert.AreEqual(true, converter.Convert("1"));
            Assert.AreEqual(true, converter.Convert("yes"));
            Assert.AreEqual(true, converter.Convert("YES"));
        }

        [Test]
        public void ConverterDatetime_Test()
        {
            IConverter converter = new ConverterDatetime();
            Assert.AreEqual(new DateTime(1997, 5, 1, 1, 3, 42), converter.Convert("01.05.1997 01:03:42"));
        }
    }
}
