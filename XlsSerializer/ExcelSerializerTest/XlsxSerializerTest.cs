﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using System.IO;
using XlsSerializer.ExcelWrapper;
using XlsSerializer;
using Moq;
using XlsSerializer.Converter;

namespace ExcelSerializerTest
{
    [TestFixture]
    class XlsxSerializerTest
    {
        [Test]
        public void XlsxSerializer_Test()
        {
            string pathfile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\Book1.xlsx");
            using(var xlsxSerializer = new XlsxSerializer(new ExcelWrp(pathfile)))
            {
                Assert.IsNotNull(xlsxSerializer);
            }
        }

        [Test]
        public void XlsxSerializer2_Test()
        {
            string pathfile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\Book1.xlsx");
            using (var xlsxSerializer = new XlsxSerializer(pathfile))
            {
                Assert.IsNotNull(xlsxSerializer);
            }
        }

        [Test]
        public void XlsxSerializer3_Test()
        {
            var repo = new MockRepository(MockBehavior.Loose);
            var excelWrp = repo.Create<IExcelWrp>();
            var creatorConverter = repo.Create<ICreatorConverter>();
            using (var xlsxSerializer = new XlsxSerializer(excelWrp.Object, creatorConverter.Object))
            {
                Assert.IsNotNull(xlsxSerializer);
            }
            excelWrp.Verify(e => e.Dispose());
        }

        [Test]
        public void XlsxSerialier_Deserialize_Test()
        {
            var repo = new MockRepository(MockBehavior.Loose);
            var excelWrp = repo.Create<IExcelWrp>();
            excelWrp.Setup(e => e.GetCell(It.IsAny<string>(), It.Is<string>(s => s == "Field1"), 1)).Returns("1");
            excelWrp.Setup(e => e.GetCell(It.IsAny<string>(), It.Is<string>(s => s == "Field2"), 1)).Returns("Name");
            excelWrp.Setup(e => e.GetCell(It.IsAny<string>(), It.Is<string>(s => s == "Field3"), 1)).Returns("1,1");
            var converter = repo.Create<IConverter>();
            converter.Setup(c => c.Convert(It.Is<string>(s => s == "1"))).Returns(1);
            converter.Setup(c => c.Convert(It.Is<string>(s => s == "1,1"))).Returns((decimal)1.1);
            converter.Setup(c => c.Convert(It.Is<string>(s => s == "Name"))).Returns("Name");
            var creatorConverter = repo.Create<ICreatorConverter>();
            creatorConverter.Setup(c => c.Create(It.IsAny<Type>())).Returns(converter.Object);

            using (var xlsxSerializer = new XlsxSerializer(excelWrp.Object, creatorConverter.Object))
            {
                Sheet1 sheet1 = xlsxSerializer.Deserialize<Sheet1>();
                Assert.AreEqual(1, sheet1.Field1);
                Assert.AreEqual("Name", sheet1.Field2);
                Assert.AreEqual(1.1, sheet1.Field3);
            }
        }

        [Test]
        public void XlsxSerialier_Deserialize_FuncTest()
        {
            var repo = new MockRepository(MockBehavior.Loose);
            var excelWrp = repo.Create<IExcelWrp>();
            excelWrp.Setup(e => e.GetCell(It.IsAny<string>(), It.Is<string>(s => s == "Field1"), 1)).Returns("1");
            excelWrp.Setup(e => e.GetCell(It.IsAny<string>(), It.Is<string>(s => s == "Field2"), 1)).Returns("Name");
            excelWrp.Setup(e => e.GetCell(It.IsAny<string>(), It.Is<string>(s => s == "Field3"), 1)).Returns("1,1");

            using (var xlsxSerializer = new XlsxSerializer(excelWrp.Object))
            {
                Sheet1 sheet1 = xlsxSerializer.Deserialize<Sheet1>();
                Assert.AreEqual(1, sheet1.Field1);
                Assert.AreEqual("Name", sheet1.Field2);
                Assert.AreEqual(1.1, sheet1.Field3);
            }
        }

        [Test]
        public void XlsxSerialier_Deserialize_IntegrTest()
        {
            string pathfile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\Book1.xlsx");

            using (var xlsxSerializer = new XlsxSerializer(pathfile))
            {
                Sheet1 sheet1 = xlsxSerializer.Deserialize<Sheet1>();
                Assert.AreEqual(1, sheet1.Field1);
                Assert.AreEqual("String1", sheet1.Field2);
                Assert.AreEqual(55, sheet1.Field3);
            }
        }

        [Test]
        public void XlsxSerialier_Deserialize12_IntegrTest()
        {
            string pathfile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\Book1.xlsx");

            using (var xlsxSerializer = new XlsxSerializer(pathfile))
            {
                Sheet1 sheet1 = xlsxSerializer.Deserialize<Sheet1>(2);
                Assert.AreEqual(3, sheet1.Field1);
                Assert.AreEqual("Name", sheet1.Field2);
                Assert.AreEqual((decimal)66.8, sheet1.Field3);
            }
        }

        [Test]
        public void XlsxSerialier_DeserializeList_IntegrTest()
        {
            string pathfile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\Book1.xlsx");

            using (var xlsxSerializer = new XlsxSerializer(pathfile))
            {
                IEnumerable<Sheet1> sheets = xlsxSerializer.DeserializeList<Sheet1>();
                Assert.AreEqual(2, sheets.Count());
                Assert.AreEqual("String1", sheets.First().Field2);
                Assert.AreEqual(55, sheets.First().Field3);
                Assert.AreEqual((decimal)66.8, sheets.Last().Field3);
            }
        }

        [Test]
        public void XlsxSerialier_DeserializeList_CalcField_IntegrTest()
        {
            string pathfile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\Invoice.xlsx");
            using (var xlsxSerializer = new XlsxSerializer(pathfile))
            {
                IEnumerable<Incoming> incomcings = xlsxSerializer.DeserializeList<Incoming>();
                Assert.AreEqual(5, incomcings.Count());
                Assert.AreEqual(1, incomcings.First().Id);
                Assert.AreEqual("good1", incomcings.First().Description);
                Assert.AreEqual(1, incomcings.First().Qnty);
                Assert.AreEqual(3, incomcings.First().Price);
                Assert.AreEqual(3, incomcings.First().Amount);

                Assert.AreEqual(5, incomcings.Last().Id);
                Assert.AreEqual("good5", incomcings.Last().Description);
                Assert.AreEqual((decimal)5.1, incomcings.Last().Qnty);
                Assert.AreEqual((decimal)1.1, incomcings.Last().Price);
                Assert.AreEqual((decimal)5.61, incomcings.Last().Amount);

            }
        }

        [Test]
        public void XlsxSerialier_Deserialize_Date_IntegrTest()
        {
            string pathfile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\Book1.xlsx");

            using (var xlsxSerializer = new XlsxSerializer(pathfile))
            {
                Sheet2 sheets = xlsxSerializer.Deserialize<Sheet2>();
                Assert.AreEqual(new DateTime(1997, 5, 1, 0, 0, 0), sheets.F3);
            }
        }
    }
}
