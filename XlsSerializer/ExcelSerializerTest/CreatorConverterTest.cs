﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using XlsSerializer.Reflector;
using XlsSerializer.Converter;

namespace ExcelSerializerTest
{
    [TestFixture]
    class CreatorConverterTest
    {
        [Test]
        public void CreatorConverterInt_Test()
        {
            ICreatorConverter creator = new CreatorConveterDefault();
            IConverter converterInt = creator.Create(typeof(int));
            Assert.IsInstanceOf(typeof(ConverterInt), converterInt);

        }

        [Test]
        public void ConverterString_Test()
        {
            ICreatorConverter creator = new CreatorConveterDefault();
            IConverter converter = creator.Create(typeof(string));
            Assert.IsInstanceOf(typeof(ConverterString), converter);
        }

        [Test]
        public void ConverterDecimal_Test()
        {
            ICreatorConverter creator = new CreatorConveterDefault();
            IConverter converter = creator.Create(typeof(decimal));
            Assert.IsInstanceOf(typeof(ConverterDecimal), converter);
        }

        [Test]
        public void ConverterBoolean_Test()
        {
            ICreatorConverter creator = new CreatorConveterDefault();
            IConverter converter = creator.Create(typeof(bool));
            Assert.IsInstanceOf(typeof(ConverterBoolean), converter);
        }

        [Test]
        public void ConverterDatetime_Test()
        {
            ICreatorConverter creator = new CreatorConveterDefault();
            IConverter converter = creator.Create(typeof(DateTime));
            Assert.IsInstanceOf(typeof(ConverterDatetime), converter);
        }
    }
}
