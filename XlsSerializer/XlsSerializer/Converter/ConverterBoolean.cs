﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XlsSerializer.Converter
{
    internal class ConverterBoolean : IConverter
    {
        public object Convert(string source)
        {
            string upperCase = source.Trim().ToUpper();
            return upperCase == "YES" || upperCase == "TRUE" || upperCase == "1";
        }
    }
}
