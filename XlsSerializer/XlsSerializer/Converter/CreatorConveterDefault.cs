﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XlsSerializer.Converter
{
    public class CreatorConveterDefault : ICreatorConverter
    {
        public IConverter Create(Type type)
        {
            if (type.Equals(typeof(int))) return new ConverterInt();
            if (type.Equals(typeof(string))) return new ConverterString();
            if (type.Equals(typeof(decimal))) return new ConverterDecimal();
            if (type.Equals(typeof(bool))) return new ConverterBoolean();
            if (type.Equals(typeof(DateTime))) return new ConverterDatetime();

            throw new NotImplementedException(string.Format("The type {0} is not implemented", type.Name));
        }
    }
}
