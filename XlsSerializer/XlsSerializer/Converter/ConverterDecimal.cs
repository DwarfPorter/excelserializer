﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XlsSerializer.Converter
{
    internal class ConverterDecimal : IConverter
    {
        public object Convert(string source)
        {
            return decimal.Parse(source);
        }
    }
}
