﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XlsSerializer.Converter
{
    public interface IConverter
    {
        object Convert(string source);
    }
}
