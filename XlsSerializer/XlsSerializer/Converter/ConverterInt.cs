﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XlsSerializer.Converter
{
    internal class ConverterInt : IConverter
    {
        public object Convert(string source)
        {
            return int.Parse(source);
        }
    }
}
