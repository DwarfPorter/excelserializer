﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XlsSerializer.Converter;
using XlsSerializer.ExcelWrapper;
using XlsSerializer.Reflector;

namespace XlsSerializer
{
    public class XlsxSerializer: IDisposable
    {
        private IExcelWrp excelWrp;
        private ICreatorConverter creatorConverter; 

        public XlsxSerializer(string fileName):this(new ExcelWrp(fileName), new CreatorConveterDefault())
        { }

        internal XlsxSerializer(IExcelWrp excelWrp):this(excelWrp, new CreatorConveterDefault())
        {
        }

        internal XlsxSerializer(IExcelWrp excelWrp, ICreatorConverter creatorConverter)
        {
            this.excelWrp = excelWrp;
            this.creatorConverter = creatorConverter;
        }

        public void Dispose()
        {
            excelWrp.Dispose();
        }

        public T Deserialize<T>()  where T :new()
        {
            return Deserialize<T>(1);
        }

        public T Deserialize<T>(int row) where T : new()
        {
            var propertyCollector = new PropertyCollector<T>();
            var className = new ClassName<T>();
            return PopulateObject<T>(propertyCollector, className, row);
        }

        private T PopulateObject<T>(PropertyCollector<T> propertyCollector, ClassName<T> className, int row) where T : new()
        {
            var obj = new T();
            foreach (var item in propertyCollector.Properties)
            {
                string value = excelWrp.GetCell(className.Name, item.Name, row);
                var converter = creatorConverter.Create(item.PropertyType);
                item.SetValue(obj, converter.Convert(value));
            }
            return obj;
        }

        public IEnumerable<T> DeserializeList<T>() where T : new()
        {
            var answer = new List<T>();
            var className = new ClassName<T>();
            var propertyCollector = new PropertyCollector<T>();
            var lastRow = excelWrp.CountRow(className.Name);
            for(var i = 1; i <= lastRow; i++)
            {
                answer.Add(PopulateObject(propertyCollector, className, i));
            }
            return answer;
        }
    }
}
