﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XlsSerializer.Reflector
{
    internal class ClassName<T>
    {
        private Type typeObject;
        private string _name = string.Empty;
        public string Name
        {
            get
            {
                if (string.IsNullOrEmpty(_name))
                {
                    _name = typeObject.Name;
                }
                return _name;
            }
        }


        public ClassName()
        {
            typeObject = typeof(T);
        }


    }
}
