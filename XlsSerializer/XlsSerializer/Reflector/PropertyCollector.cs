﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace XlsSerializer.Reflector
{
    internal class PropertyCollector<T>
    {
        private Type typeObject;
        private IEnumerable<PropertyInfo> _propeties = Enumerable.Empty<PropertyInfo>();
        public IEnumerable<PropertyInfo> Properties
        {
            get
            {
                if (!_propeties.Any())
                {
                    _propeties = typeObject.GetProperties(BindingFlags.Instance | BindingFlags.Public).Where(p => p.CanWrite).Where(p => p.SetMethod.IsPublic);
                }
                return _propeties;
            }
        }
        
        public PropertyCollector()
        {
            typeObject = typeof(T);
        }

    }
}
