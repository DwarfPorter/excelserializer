﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace XlsSerializer.ExcelWrapper
{
    internal class ExcelWrp : IExcelWrp
    {
        private const bool Visible = false;
        private const int HeaderRow = 0;

        private string pathFile;
        private Excel.Application exclApp;
        private Excel.Workbook exclBook;
        private List<Excel.Worksheet> _exclSheets = new List<Excel.Worksheet>();
        private List<Excel.Worksheet> ExclSheets
        {
            get
            {
                if (!_exclSheets.Any())
                {
                    foreach (Excel.Worksheet item in exclBook.Sheets)
                    {
                        _exclSheets.Add(item);
                    }
                }
                return _exclSheets;
            }
        }

        public int CountRow(string sheetName)
        {
            var currentSheet = GetSheetByName(sheetName);
            return currentSheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell).Row - 1;
        }

        private Dictionary<string, int> mapFields = new Dictionary<string, int>();
        private Dictionary<string, int> GetMapFields(Excel.Worksheet currentSheet)
        {
            if (!mapFields.Any())
            {
                var lastColumn = currentSheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell).Column;
                for (int i = 0; i < lastColumn; i++)
                {
                    string key = GetCell(currentSheet, HeaderRow, i);
                    mapFields.Add(key, i);
                }
            }
            return mapFields;
        }


        public ExcelWrp(string pathFile)
        {
            this.pathFile = pathFile;
            exclApp = new Excel.Application();
            exclApp.Visible = Visible;
            exclBook = exclApp.Workbooks.Open(this.pathFile);
        }

        public IEnumerable<string> GetSheets()
        {
            return ExclSheets.Select(s => s.Name);
        }

        public void Dispose()
        {
            exclApp.Quit();
        }

        public string GetFileName()
        {
            return exclBook.Name;
        }

        public int GetSheet(string sheetName)
        {
            int number = 0;
            foreach (var item in GetSheets())
            {
                
                if (item == sheetName) return number;
                number++;
            }
            throw new ArgumentOutOfRangeException("sheetName", "Sheet does not found in excel book.");
        }

        public string GetCell(string sheetName, int row, int column)
        {
            var currentSheet = GetSheetByName(sheetName);
            return GetCell(currentSheet, row, column);
        }

        private Excel.Worksheet GetSheetByName(string sheetName)
        {
            return ExclSheets.Single(s => s.Name == sheetName);
        }

        private string GetCell(Excel.Worksheet currentSheet, int row, int column)
        {
            return currentSheet.Cells[row + 1, column + 1].Value.ToString();
        }

        public string GetCell(string sheetName, string fieldName, int row)
        {
            var currentSheet = GetSheetByName(sheetName);
            var currentField = GetFieldName(currentSheet, fieldName);
            return GetCell(currentSheet, row, currentField);
        }

        private int GetFieldName(Excel.Worksheet currentSheet, string fieldName)
        {
            var lastColumn = currentSheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell).Column;
            try
            {
                return GetMapFields(currentSheet)[fieldName];
            }
            catch(KeyNotFoundException e)
            {
                throw new ArgumentOutOfRangeException("Field does not found in excel book.", e); 
            }
        }
    }
}
