﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XlsSerializer.ExcelWrapper
{
    public interface IExcelWrp : IDisposable
    {
        int CountRow(string sheetName);
        IEnumerable<string> GetSheets();
        string GetFileName();
        int GetSheet(string sheetName);
        string GetCell(string sheetName, int row, int column);
        string GetCell(string sheetName, string fieldName, int row);
    }
}
